// Entorno
process.env.PORT = process.env.PORT || 3000

process.env.NODE_ENV = process.env.NODE_ENV || 'dev'

// DB
let urlDB

if (process.env.NODE_ENV === 'dev') {
  urlDB = 'mongodb://localhost:27017/cafe'
} else {
  urlDB = process.env.URLDB
}

process.env.URLDB = urlDB

// token
process.env.CADUCIDAD_TOKEN = "30 days" || 60 * 60 * 24 * 30

// seed 
process.env.SEED = process.env.SEED || 'mi-secreto-desarrollo'
// heroku config:set SEED="valor"

// google client ID

process.env.CLIENT_ID = process.env.CLIENT_ID || '236628968041-38tj7n5qd9767p7jqngc1ht8klispiat.apps.googleusercontent.com'