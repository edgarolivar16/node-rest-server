const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const path = require('path')
const app = express()
require('./config/config')
const puerto = process.env.PORT
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: false
}))

// parse application/json
app.use(bodyParser.json())

// habilitar la carpeta public
app.use(express.static(path.resolve(__dirname,'../public')))
// configuracion general de rutas
app.use(require('./routes/index'));


mongoose.connect(process.env.URLDB, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  })
  .then(console.log(`DB online`))
  .catch(console.log)

app.listen(puerto, () => {
  console.log(`Server on port: ${puerto}`)
})