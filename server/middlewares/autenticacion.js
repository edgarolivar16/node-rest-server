const jwt = require('jsonwebtoken')

let verificaToken = async (req, res, next) => {
  // leemos el header
  let token = req.get('token')
  jwt.verify(token, process.env.SEED, (err, payload) => {
    if (err) {
      return res.status(401).json({
        success: false,
        err
      })
    }
    req.usuario = payload.user
    next()
  })
}
let verificaAdminRol = (req, res, next) => {
  let usuario = req.usuario

  if (usuario.role === 'ADMIN_ROLE') {
    next()
  }else{
    res.json({
      success: false,
      err: {
        message: 'El usuario no es administrador'
      }
    })
  }
}

let verificaTokenImg = (req,res,next)=>{
  let token = req.query.token
  jwt.verify(token, process.env.SEED, (err, payload) => {
    if (err) {
      return res.status(401).json({
        success: false,
        err
      })
    }
    req.usuario = payload.user
    next()
  })


  
}

function parseJwt (token) {
  var base64Url = token.split('.')[1];
  var base64 = base64Url.replace('-', '+').replace('_', '/');
  return JSON.parse(window.atob(base64));
};

module.exports = {
  verificaToken,
  verificaAdminRol,
  verificaTokenImg
}