const express = require('express')
const app = express()
app.use(require('./usuario'));
app.use(require('./login'));
app.use(require('./categoria'));
app.use(require('./producto'));
app.use(require('./upload'));
app.use(require('./imagen'));

module.exports = app

/**
 * app.use('/producto', require('./producto')):
 * si lo hacemos de la forma de arriba le habilitamos la palabra
 * '/producto' a todas las rutas que se encuentren en el archivo 
 * producto.js
 */