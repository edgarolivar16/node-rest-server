const express = require('express')
const app = express()
const bcrypt = require('bcrypt');
const Usuario = require('../models/usuario')
const _ = require('underscore')
const {
  verificaToken,
  verificaAdminRol
} = require('../middlewares/autenticacion')

app.get('/usuario', verificaToken, async (req, res) => {

  // return res.json({
  //   success: true,
  //   usuario: req.usuario
  // })
  let desde = Number(req.query.desde) || 0
  let limite = Number(req.query.limite) || 5
  let retorno = {}

  Usuario.find({
      estado: true
    }, 'nombre email role estado google')
    .skip(desde)
    .limit(limite)
    .then(usuarios => {
      retorno.success = true
      retorno.usuarios = usuarios
      return Usuario.countDocuments({
        estado: true
      })

      // res.send({
      //   success: true,
      //   usuarios
      // })
    })
    .then(conteo => {
      retorno.conteo = conteo
      res.send(retorno)
    })
    .catch(err => {
      res.send({
        success: false,
        err
      })
    })
  // .exec((err, usuarios) => {
  //   if (err) {
  //     return res.status(400).json({
  //       success: false,
  //       err
  //     })
  //   }

  //   res.json({
  //     success: true,
  //     usuarios
  //   })
  // })

})

app.post('/usuario', [verificaToken, verificaAdminRol], (req, res) => {
  const body = req.body

  let usuario = new Usuario({
    nombre: body.nombre,
    email: body.email,
    password: bcrypt.hashSync(body.password, 10),
    role: body.role
  })

  usuario.save()
    .then(() => {
      res.json({
        success: true
      })
    })
    .catch(err => {
      res.status(400).json({
        success: false,
        err
      })
    })
})

app.put('/usuario/:id', [verificaToken, verificaAdminRol], (req, res) => {
  let id = req.params.id
  let body = _.pick(req.body, ['nombre', 'email', 'img', 'role', 'estado'])

  // esta forma es ineficiente....
  // delete body.password
  // delete body.google


  Usuario.findByIdAndUpdate(id, body, {
    new: true,
    runValidators: true
  }, (err, usuarioDB) => {
    if (err) {
      return res.status(400).json({
        success: false,
        err
      })
    } else {
      res.json({
        success: true,
        usuario: usuarioDB
      })

    }

  })
})

app.delete('/usuario/:id', [verificaToken, verificaAdminRol], (req, res) => {
  /**
   * actualmente no se piensa en borrar datos, si no en darlos de baja
   */

  let id = req.params.id

  Usuario.findByIdAndUpdate(id, {
      estado: false
    }, {
      new: true
    })
    .then(usuario => {
      usuario = usuario != null ? usuario : `Usuario no encontrado`
      res.send({
        success: true,
        usuario
      })
    })
    .catch(err => {
      res.status(400).send({
        success: false,
        err
      })
    })

  // Usuario.findByIdAndRemove(id)
  //   .then(usuario => {
  //     usuario = usuario != null ? usuario : `Usuario no encontrado`
  //     res.send({
  //       success: true,
  //       usuario
  //     })
  //   })
  //   .catch(err => {
  //     res.status(400).send({
  //       success: false,
  //       err
  //     })
  //   })


})

module.exports = app