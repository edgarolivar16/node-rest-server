const express = require('express')
let {
  verificaToken,
  verificaAdminRol
} = require('../middlewares/autenticacion')
let app = express()
let Categoria = require('../models/categoria')

// mostrar todas categorias
app.get('/categoria', verificaToken, (req, res) => {
  Categoria
    .find({})
    .sort({descripcion: 1})
    .populate('usuario', 'nombre email')
    .exec((err, categorias) => {

      if (err) {
        return res.status(400).json({
          success: false,
          err
        })
      }

      res.json({
        success: true,
        categorias
      })

    })
})

app.get('/categoria/:id', verificaToken, (req, res) => {
  let id = req.params.id

  Categoria.findById(id, (err, categoriaDB) => {

    if (err) {
      return res.status(400).json({
        success: false,
        err
      })
    }

    if (!categoriaDB) {
      return res.status(400).json({
        success: false,
        err: {
          message: 'El id no existe'
        }
      })
    }

    res.json({
      success: true,
      categoria: categoriaDB
    })

  })
})

app.post('/categoria', verificaToken, (req, res) => {
  let body = req.body

  let categoria = new Categoria({
    descripcion: body.descripcion,
    usuario: req.usuario._id
  })

  categoria.save((err, categoriaDB) => {
    if (err) {
      return res.status(400).json({
        success: false,
        err
      })
    }

    if (!categoriaDB) {
      return res.status(400).json({
        success: false,
        err
      })
    }

    res.json({
      success: true,
      categoria: categoriaDB
    })


  })
})

app.put('/categoria/:id', verificaToken, (req, res) => {
  let id = req.params.id
  let body = req.body
  let descCategoria = {
    descripcion: body.descripcion
  }
  Categoria.findByIdAndUpdate(id, descCategoria, {
    new: true,
    runValidators: true
  }, (err, categoriaDB) => {
    if (err) {
      return res.status(400).json({
        success: false,
        err
      })
    }

    if (!categoriaDB) {
      return res.status(400).json({
        success: false,
        err: {
          message: 'El id no existe'
        }
      })
    }

    res.json({
      success: true,
      categoria: categoriaDB
    })
  })

})

app.delete('/categoria/:id', [verificaToken, verificaAdminRol], (req, res) => {
  let id = req.params.id
  Categoria.findByIdAndRemove(id, (err, categoriaDB) => {
    if (err) {
      return res.status(400).json({
        success: false,
        err
      })
    }

    if (!categoriaDB) {
      return res.status(400).json({
        success: false,
        err: {
          message: 'El id no existe'
        }
      })
    }

    res.json({
      success: true,
      message: 'Categoria borrada'
    })
  })
})

module.exports = app