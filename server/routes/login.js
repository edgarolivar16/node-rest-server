const express = require('express')
const bcrypt = require('bcrypt');
const Usuario = require('../models/usuario')
const app = express()
const jwt = require('jsonwebtoken')
const {
  OAuth2Client
} = require('google-auth-library');
const client = new OAuth2Client(process.env.CLIENT_ID);

app.post('/login', async (req, res) => {
  let body = req.body

  let user = await Usuario.findOne({
    email: body.email
  })

  if (user) {

    if (bcrypt.compareSync(body.password, user.password)) {
      let token = jwt.sign({
        user
      }, process.env.SEED, {
        expiresIn: process.env.CADUCIDAD_TOKEN
      })
      res.json({
        success: true,
        user,
        token
      })
    } else {
      res.status(400).json({
        success: false,
        err: {
          message: 'Credenciales erroneas 2'
        }
      })
    }


  } else {
    res.status(400).json({
      success: false,
      err: {
        message: 'Credenciales erroneas'
      }
    })
  }
})


// configuracion de google
async function verify(token) {
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: process.env.CLIENT_ID
  });
  const payload = ticket.getPayload();
  const {
    name,
    email,
    picture
  } = payload
  return {
    nombre: name,
    email,
    picture,
    google: true
  }

}

app.post('/google', async (req, res) => {
  let token = req.body.idtoken
  let googleUser = await verify(token).catch(err => {
    res.status(403).json({
      success: false,
      err
    })
  })
  // res.send({
  //   usuario: googleUser
  // })
// console.log(googleUser);
  Usuario.findOne({
    email: googleUser.email
  }, (err, usuarioDB) => {
    if (err) {
      return res.status(500).json({
        success: false,
        err
      })
    }

    if (usuarioDB) {
      if (usuarioDB.google === false) {
        return res.status(400).json({
          success: false,
          err: 'Debe de usar su autenticacion normal'
        })
      } else {
        // renovamos el token
        let token = jwt.sign({
          usuarioDB
        }, process.env.SEED, {
          expiresIn: process.env.CADUCIDAD_TOKEN
        })
        return res.json({
          success: true,
          usuario: usuarioDB,
          token
        })
      }
    } else {
      // si el usuario no existe
      
      let usuario = new Usuario()
      usuario.nombre = googleUser.nombre
      usuario.email = googleUser.email
      usuario.img = googleUser.picture
      usuario.google = googleUser.google
      // el password no va a ser requerido, pero debe pasar la validacion del modelo, no entrara con una carita
      usuario.password = bcrypt.hashSync('123456', 10)

      usuario.save((err, usuarioDB) => {
        if (err) return res.status(500).json({
          success: false,
          err
        })
        let token = jwt.sign({
          usuario: usuarioDB
        }, process.env.SEED, {
          expiresIn: process.env.CADUCIDAD_TOKEN
        })

        return res.json({
          success: true,
          usuario: usuarioDB,
          token
        })

      })
    }
  })
})


module.exports = app