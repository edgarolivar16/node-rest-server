const express = require('express')
let {
  verificaToken,
  verificaAdminRol
} = require('../middlewares/autenticacion')
let app = express()
let Producto = require('../models/producto')

app.get('/producto', verificaToken, (req, res) => {
  // trae los productos 
  // popilate usuario categoria
  // paginado
  let desde = req.query.desde || 0
  desde = Number(desde)
  Producto
    .find({
      disponible: true
    })
    .skip(desde)
    .limit(5)
    .populate('usuario', 'nombre email')
    .populate('categoria')
    // .populate('usuario categoria')
    .exec((err, productos) => {
      if (err) {
        return res.status(500).json({
          success: false,
          err
        })
      }

      res.status(201).json({
        success: true,
        productos
      })
    })

})

app.get('/producto/:id', (req, res) => {
  let id = req.params.id
  Producto
    .findById(id)
    .populate('usuario')
    .populate('categoria')
    .exec((err, productoDB) => {
      if (err) {
        return res.status(400).json({
          success: false,
          err
        })
      }

      if (!productoDB) {
        return res.status(400).json({
          success: false,
          err: {
            message: 'El id no existe'
          }
        })
      }

      res.json({
        success: true,
        producto: productoDB
      })
    })
  // popilate usuario categoria
})

// buscar por nombre
app.get('/producto/buscar/:termino', verificaToken, (req, res) => {
  let termino = req.params.termino

  // la i es para que sea insensible a mayusculas - minusculas
  let regex = new RegExp(termino, 'i')
  Producto
    .find({
      nombre: regex,
      disponible: true
    })
    .populate('categoria', 'nombre')
    .exec((err, productos) => {
      if (err) {
        return res.status(500).json({
          success: false,
          err
        })
      }

      res.status(201).json({
        success: true,
        productos
      })
    })

})

app.post('/producto', verificaToken, (req, res) => {


  // grabar el usuairo
  // rabar una categoria

  let {
    nombre,
    precioUni,
    descripcion,
    categoria
  } = req.body

  let producto = new Producto({
    nombre,
    precioUni,
    descripcion,
    categoria,
    usuario: req.usuario._id
  })
  producto.save((err, productoDB) => {
    if (err) {
      return res.status(500).json({
        success: false,
        err
      })
    }

    res.status(201).json({
      success: true,
      producto: productoDB
    })
  })

})
app.put('/producto/:id', (req, res) => {
  // grabar el usuario
  // grabar una categoria

  let id = req.params.id
  let body = req.body

  Producto.findByIdAndUpdate(id, body, {
    new: true,
    runValidators: true
  }, (err, productoDB) => {
    if (err) {
      return res.status(400).json({
        success: false,
        err
      })
    }

    if (!productoDB) {
      return res.status(400).json({
        success: false,
        err: {
          message: 'El id no existe'
        }
      })
    }

    res.json({
      success: true,
      producto: productoDB
    })
  })
})
app.delete('/producto/:id', verificaToken, (req, res) => {
  // disponible = false
  let id = req.params.id

  Producto.findByIdAndUpdate(id, {
    disponible: false
  }, {
    new: true,
    runValidators: true
  }, (err, productoDB) => {
    if (err) {
      return res.status(400).json({
        success: false,
        err
      })
    }

    if (!productoDB) {
      return res.status(400).json({
        success: false,
        err: {
          message: 'El id no existe'
        }
      })
    }

    res.json({
      success: true,
      producto: productoDB
    })

  })
})


module.exports = app