const express = require('express')
const app = express()
const fileUpload = require('express-fileupload')
const Usuario = require('../models/usuario')
const Producto = require('../models/producto')
const fs = require('fs')
const path = require('path')
let {
  verificaToken,
  verificaTokenImg
} = require('../middlewares/autenticacion')

app.use(fileUpload({
  useTempFiles: true
}))

app.get('/imagen/:tipo/:img', verificaTokenImg, (req, res) => {
  let {
    tipo,
    img
  } = req.params

  let pathImgen = path.resolve(__dirname, `../../uploads/${tipo}/${img}`)

  if (fs.existsSync(pathImgen)) {

    res.sendFile(pathImgen)

  } else {
    let noImagePath = path.resolve(__dirname, '../assets/no-img-found.jpg')
    res.sendFile(noImagePath)

  }

})

module.exports = app