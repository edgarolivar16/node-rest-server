const express = require('express')
const app = express()
const fileUpload = require('express-fileupload')
const Usuario = require('../models/usuario')
const Producto = require('../models/producto')
const fs = require('fs')
const path = require('path')
let {
  verificaToken,
  verificaAdminRol
} = require('../middlewares/autenticacion')

app.use(fileUpload({
  useTempFiles: true
}))

// esto agrega el req.files

app.put('/upload/:tipo/:id', verificaToken, (req, res) => {
  if (!req.files || Object.keys(req.files).length === 0) {
    return res.status(400).json({
      success: false,
      err: {
        message: 'No se envio ningun archivo'
      }
    });
  }

  let {
    tipo,
    id
  } = req.params
  // valida tipo
  let tiposValidos = ['producto', 'usuario']
  if (tiposValidos.indexOf(tipo) < 0) {
    return res.status(400).json({
      success: false,
      err: {
        message: 'Los tipos validos son: ' + tiposValidos.join(', ')
      }
    })
  }


  let archivo = req.files.archivo
  let nombreCortado = archivo.name.split('.')
  let extension = nombreCortado[nombreCortado.length - 1].toLowerCase()

  // restrincion del tipo de archivo
  let extensionesValidas = ['png', 'jpg', 'gif', 'jpeg']

  if (extensionesValidas.indexOf(extension) < 0) {
    return res.status(400).json({
      success: false,
      err: {
        message: 'Archivo no valido, solo ' + extensionesValidas.join(', ')
      }
    })
  }

  // cambiar nombre al archivo

  let nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extension}`


  archivo.mv(`uploads/${tipo}/${nombreArchivo}`, (err) => {
    if (err)
      return res.status(500).json({
        success: false,
        err
      });

    if (tipo == 'producto') imagenProducto(id, res, nombreArchivo)
    else imagenUsuario(id, res, nombreArchivo)
  });


})


function imagenUsuario(id, res, nombreArchivo) {
  Usuario.findById(id, (err, usuarioDB) => {
    if (err) {
      // esto para prevenir que se este almacenando basura en caso de un id erroneo
      borrarArchivo(nombreArchivo, 'usuario')
      return res.status(500).json({
        success: false,
        err
      })
    }

    if (!usuarioDB) {
      return res.status(400).json({
        success: false,
        err: {
          message: 'usuario no encontrado'
        }
      })
    }

    borrarArchivo(usuarioDB.img, 'usuario')

    usuarioDB.img = nombreArchivo

    usuarioDB.save((err, usuarioGuardado) => {

      res.json({
        success: true,
        usuario: usuarioGuardado,
        img: nombreArchivo
      })
    })


  })
}

function imagenProducto(id, res, nombreArchivo) {
  Producto.findById(id, (err, productoDB) => {
    if (err) {
      // esto para prevenir que se este almacenando basura en caso de un id erroneo
      borrarArchivo(nombreArchivo, 'producto')
      return res.status(500).json({
        success: false,
        err
      })
    }

    if (!productoDB) {
      return res.status(400).json({
        success: false,
        err: {
          message: 'producto no encontrado'
        }
      })
    }

    borrarArchivo(productoDB.img, 'producto')

    productoDB.img = nombreArchivo

    productoDB.save((err, productoGuardado) => {

      res.json({
        success: true,
        producto: productoGuardado,
        img: nombreArchivo
      })
    })


  })
}

function borrarArchivo(nombreImagen, tipo) {

  let pathImagen = path.resolve(__dirname, `../../uploads/${tipo}/${nombreImagen}`)

  if (fs.existsSync(pathImagen)) {
    fs.unlinkSync(pathImagen)
  }

}


module.exports = app